from random import randint

name = input("Hi! What is your name? ")
#The for loop assigns the current value from range to the variable guess_number.
    #So, it'll do Guess 1, Guess 2, Guess 3, etc. etc. 
for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess", guess_number, name, "were you born in",
        month_number, "/", year_number, "?")

    response = input("yes or no? ")
#If the person responds with "yes", then the computer will 
    #print "I knew it" and exit.
    if response == "yes":
        print("I knew it!")
        exit()
#Otherwise, they answered no. If the guess is equal to the fifth guess, we are done.
    elif guess_number == 5:
        print("I have other things to do. Goodbye.")
#The person's response is no and we aren't at guess 5 yet. Try again.
    else:
        print("Drat! Lemme try again!")

